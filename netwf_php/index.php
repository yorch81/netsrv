<?php
    require './vendor/autoload.php';
    require 'Data.class.php';
    require 'config.php';

    \Slim\Slim::registerAutoloader();

    MyDb::getConnection('MySQLDb', $cfg_host, $cfg_user, $cfg_pwd, $cfg_db, $cfg_port);

    $web = new \Slim\Slim();

    $web->get(
            '/',
            function () {
                $app = \Slim\Slim::getInstance();
                
                $app->response()->status(200);
                $app->response()->header('Content-Type', 'application/json');

                $msg = array("msg" => "Welcome to netWf PHP Core !!!");

                echo json_encode($msg);
            }
    );

    $web->get(
            '/api/table/(:name)/(:limit)',
            function () {
                $app = \Slim\Slim::getInstance();
                
                $app->redirect('/notimplemented');
            }
    );

    $web->get(
            '/api/tablebyid/(:name)/(:fieldkey)/(:key)',
            function () {
                $app = \Slim\Slim::getInstance();
                
                $app->redirect('/notimplemented');
            }
    );

    $web->get(
            '/api/first/(:table)/(:fieldkey)/(:key)/(:where)',
            function () {
                $app = \Slim\Slim::getInstance();
                
                $app->redirect('/notimplemented');
            }
    );

    $web->get(
            '/api/next/(:table)/(:fieldkey)/(:key)/(:where)',
            function () {
                $app = \Slim\Slim::getInstance();
                
                $app->redirect('/notimplemented');
            }
    );

    $web->get(
            '/api/previous/(:table)/(:fieldkey)/(:key)/(:where)',
            function () {
                $app = \Slim\Slim::getInstance();
                
                $app->redirect('/notimplemented');
            }
    );

    $web->get(
            '/api/last/(:table)/(:fieldkey)/(:key)/(:where)',
            function () {
                $app = \Slim\Slim::getInstance();
                
                $app->redirect('/notimplemented');
            }
    );

    $web->post(
            '/api/procedure/(:sp)',
            function ($sp) use ($cfg_key){
                $app = \Slim\Slim::getInstance();

                try{
                    $key = $app->request->params('auth');

                    if (!$key){
                        $app->redirect('/notkey');
                    }
                    else{
                        if ($cfg_key == $key){
                            $body = $app->request->getBody();
                            $jsonData = json_decode($body, true);

                            $aParams =  array();

                            for ($i=1; $i <= count($jsonData); $i++) {
                                $par = "@p" . $i;

                                array_push($aParams, $jsonData[$par]);
                            }

                            $app->response()->header('Content-Type', 'application/json');

                            $result = Data::execute($sp, $jsonData);

                            if (is_null($result)){
                                $app->response()->status(206);
                                $msg = array("error" => "Mysql Error: Please check log application");

                                echo json_encode($msg); 
                            }
                            else{
                                $app->response()->status(200);

                                $aResult = array('Table' => $result);

                                echo json_encode($aResult);
                            }
                        }
                        else{
                            $app->redirect('/invalidkey');
                        }
                    }
                }
                catch (ResourceNotFoundException $e) {
                    $app->response()->status(404);
                    $app->response()->header('X-Status-Reason', $e->getMessage());
                } 
                catch (Exception $e) {
                    $app->response()->status(400);
                    $app->response()->header('X-Status-Reason', $e->getMessage());
                }
            }
    );

    $web->post(
            '/api/script/(:name)',
            function ($name) use ($cfg_key, $cfg_dir){
                $app = \Slim\Slim::getInstance();

                try{
                    $key = $app->request->params('auth');

                    if (!$key){
                        $app->redirect('/notkey');
                    }
                    else{
                        if ($cfg_key == $key){
                            $body = $app->request->getBody();
                            $jsonData = json_decode($body, true);

                            $app->response()->header('Content-Type', 'application/json');

                            $fileName = $cfg_dir . $name;

                            if (file_exists($fileName)) {
                                $app->response()->status(200);

                                $query = file_get_contents($fileName);

                                $dataArray = $jsonData;

                                foreach ($dataArray as $item => $value){
                                    $query = str_replace($item, $value, $query);
                                }

                                $db = MyDb::getConnection();
                                $result = $db->executeCommand($query);

                                $aResult = array('Table' => $result);

                                echo json_encode($aResult);
                            }
                            else {
                                $app->response()->status(206);
                                $msg = array("error" => "Script not exists");

                                echo json_encode($msg); 
                            }
                        }
                        else{
                            $app->redirect('/invalidkey');
                        }
                    }
                }
                catch (ResourceNotFoundException $e) {
                    $app->response()->status(404);
                    $app->response()->header('X-Status-Reason', $e->getMessage());
                } 
                catch (Exception $e) {
                    $app->response()->status(400);
                    $app->response()->header('X-Status-Reason', $e->getMessage());
                }
            }
    );
    // Not Sent Key
    $web->get(
        '/notkey',
        function () {
            $app = \Slim\Slim::getInstance();

            $app->response()->header('Content-Type', 'application/json');
            $app->response()->status(404);

            $msg = array("error" => "Not sent key");

            echo json_encode($msg);
        }
    );

    // Not Valid Key
    $web->get(
        '/invalidkey',
        function () {
            $app = \Slim\Slim::getInstance();

            $app->response()->header('Content-Type', 'application/json');
            $app->response()->status(404);

            $msg = array("error" => "Permission denied");
            
            echo json_encode($msg);
        }
    );

    // Not Implemented Method
    $web->get(
        '/notimplemented',
        function () {
            $app = \Slim\Slim::getInstance();

            $app->response()->header('Content-Type', 'application/json');
            $app->response()->status(200);

            $msg = array("error" => "Not implemented method");
            
            echo json_encode($msg);
        }
    );

    $web->run();
?>
