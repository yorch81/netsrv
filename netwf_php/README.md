# netDb Web Framework PHP #

## Description ##
netDb Web Framework PHP Core.

## Requirements ##
* [PHP 5.4.1 or higher](http://www.php.net/)
* [SQL Server](http://www.microsoft.com/es-es/server-cloud/products/sql-server/)
* [MySQL](http://www.mysql.com/)
* [netWf.js](http://cdn.3utilities.com/js/netwf/netWf.js)

## Developer Documentation ##
Execute phpdoc -d /path/to/directory/

## Installation ##
Execute composer.phar.

## REST API ##
Rows number by Table:
$ curl http://MYURL/api/table/TABLE_NAME/ROWS?auth=MASTER_KEY

One row by Id:
$ curl http://MYURL/api/tablebyid/TABLE_NAME/FIELD/KEY?auth=MASTER_KEY

First row:
$ curl http://MYURL/api/first/TABLE_NAME/FIELD/KEY/WHERE_CLAUSE?auth=MASTER_KEY

Next row:
$ curl http://MYURL/api/next/TABLE_NAME/FIELD/KEY/WHERE_CLAUSE?auth=MASTER_KEY

Previous row:
$ curl http://MYURL/api/previous/TABLE_NAME/FIELD/KEY/WHERE_CLAUSE?auth=MASTER_KEY

Last row:
$ curl http://MYURL/api/last/TABLE_NAME/FIELD/KEY/WHERE_CLAUSE?auth=MASTER_KEY

Execute Stored Procedure:
$ curl -H "Content-Type: application/json" -X POST -d JSON_PARAMETERS http://MYURL/api/procedure/PROCEDURE_NAME?auth=MASTER_KEY

Execute SQL Script:
$ curl -H "Content-Type: application/json" -X POST -d JSON_PARAMETERS http://MYURL/api/script/SCRIPT_NAME?auth=MASTER_KEY


## References ##
https://es.wikipedia.org/wiki/Representational_State_Transfer

P.D. Let's go play !!!