<?php
/**
 * Data 
 *
 * Data Execute Stored Procedure of PetRide Database
 *
 * Copyright 2016 Jorge Alberto Ponce Turrubiates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Data
 * @package    PetRide
 * @copyright  Copyright 2016 Jorge Alberto Ponce Turrubiates
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    1.0.0, 2016-11-21
 * @author     Jorge Alberto Ponce Turrubiates (the.yorch@gmail.com)
 */
class Data
{
	/**
	 * Excute Stored Procedure
	 * 
	 * @param  [string] $procedure  Stored Procedure Name
	 * @param  [Array]  $jsonParams JSON Parameters
	 * @return [Array]              
	 */
	public static function execute($procedure, $jsonParams)
	{
		$db = MyDb::getConnection();

		$retvalue = null;

		$query = 'CALL '  . $procedure . ' (';

        $items = count($jsonParams);

        for ($i=0; $i < $items; $i++) { 
            $query = $query . '?,';
        }
        
        if ($items > 0)
        	$query = substr($query, 0, -1);

        $query = $query . ')';

        $retvalue = $db->executeCommand($query, $jsonParams);

        return $retvalue;
	}
}
?>