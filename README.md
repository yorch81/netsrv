# net Server #

## Description ##
netDb Web Server.

## Requirements ##
* [.NET Framework](http://www.microsoft.com/es-mx/download/details.aspx?id=30653)
* [SQL Server](http://www.microsoft.com/es-es/server-cloud/products/sql-server/)
* [MySQL](http://www.mysql.com/)
* [NancyFx](http://nancyfx.org/)
* [netDb](https://bitbucket.org/yorch81/netdb)
* [netWf](https://bitbucket.org/yorch81/netwf)
* [JConfig](https://bitbucket.org/yorch81/jconfig)

## Developer Documentation ##
In the Code.

## Installation ##
Run EXE file netSrv.exe and configure application.

## Notes ##
For install dependencies, please run: Update-package -reinstall

In Windows must run as Administrator.

The directory web contains web application example.

The directory Scripts contains SQL scripts for MySQL and SQL Server.

## References ##
https://es.wikipedia.org/wiki/Representational_State_Transfer

P.D. Let's go play !!!




