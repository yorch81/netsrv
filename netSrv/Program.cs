﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// netSrv 
//
// netSrv Web Server Application
//
// Copyright 2016 Jorge Alberto Ponce Turrubiates
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace netSrv
{
    /// <summary>
    /// General Program
    /// </summary>
    class Program
    {
        /// <summary>
        /// Initialize Web Server
        /// </summary>
        /// <param name="args">
        /// Arguments No uses
        /// </param>
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to netDb Web Server !!!");
            
            JConfig.JConfig cfg = new JConfig.JConfig("config.json");

            int typeDb = netDb.netDb.MSSQLSERVER;
            string serverDb = "";
            string userDb = "";
            string password = "";
            string dbName = "";
            int dbPort = 0;
            string key = "";
            string tableUsers = "";
            int port = 0;

            if (cfg.totalKeys() >= 9)
            {
                typeDb = Convert.ToInt32(cfg.getValue("typeDb"));
                serverDb = cfg.getValue("serverDb");
                userDb = cfg.getValue("userDb");
                password = cfg.getValue("password");
                dbName = cfg.getValue("dbName");
                dbPort = Convert.ToInt32(cfg.getValue("dbPort"));

                key = cfg.getValue("key");
                port = Convert.ToInt32(cfg.getValue("port"));
                tableUsers = cfg.getValue("tableUsers");

                // Init Database Connection
                netDb.netDb.GetInstance(typeDb, serverDb, userDb, password, dbName, dbPort);

                // Init Web Services
                netWf.netWf web = new netWf.netWf(port, key, tableUsers);

                try
                {
                    // Start Web Application
                    web.start();

                    Console.WriteLine("Listening on http://localhost:" + port + " ...");
                    Console.WriteLine("Enter CTRL + C or 'quit' to Terminate Web Server");

                    string command = "";

                    while (command != "quit")
                        command = Console.ReadLine();

                    // Stop Web Application
                    web.stop();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Could not Start Web Server");
                    Console.WriteLine(e.Message);
                    Console.ReadKey();
                }
            }
            else
            {
                Console.WriteLine("Incorrect Configuration, please enter a valid configuration.");

                Console.Write("Enter Database Type (1 MSSQLSERVER 2 MYSQL): ");
                string strTypeDb = Console.ReadLine();
                cfg.setKey("typeDb", strTypeDb);

                Console.Write("Enter Server Name: ");
                serverDb = Console.ReadLine();
                cfg.setKey("serverDb", serverDb);

                Console.Write("Enter User: ");
                userDb = Console.ReadLine();
                cfg.setKey("userDb", userDb);

                Console.Write("Enter Password: ");
                password = Console.ReadLine();
                cfg.setKey("password", password);

                Console.Write("Enter DataBase Name: ");
                dbName = Console.ReadLine();
                cfg.setKey("dbName", dbName);

                Console.Write("Enter Database Port: ");
                string strDbPort = Console.ReadLine();
                cfg.setKey("dbPort", strDbPort);

                Console.Write("Enter Master Key: ");
                key = Console.ReadLine();
                cfg.setKey("key", key);

                Console.Write("Enter Web Service Port: ");
                string strPort = Console.ReadLine();
                cfg.setKey("port", strPort);

                Console.Write("Enter Table User Logins: ");
                tableUsers = Console.ReadLine();
                cfg.setKey("tableUsers", tableUsers);

                cfg.save();

                Console.WriteLine("Your settings were saved, press any key and restart Application ...");
                Console.ReadKey();
            }
        }
    }
}
