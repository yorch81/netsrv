/**
 * Manage Buttons Bar
 */
function netBar(){
	table = '';
	status = 1;
}

/**
 * Set Table of Bar
 * 
 * @param string tableName Table Name
 */
netBar.setTable = function(tableName){
	table = tableName;
}

/**
 * Get Table of Bar
 * @return string Table Name
 */
netBar.getTable = function(){
	return table;
}

/**
 * Set Status of Bar
 * 1 = Navigating
 * 2 = Adding
 * 3 = Editing
 * @param int statusNumber Status Number
 */
netBar.setStatus = function(statusNumber){
	status = statusNumber;
}

/**
 * Get current Staus
 * @return int Status Number
 */
netBar.getStatus = function(){
	return status;
}

/**
 * Initialize Buttons
 */
netBar.initialize = function(){
	// Navigating
	netBar.setStatus(1);

	$(".netbar-btn-first").click(function() {
        api.first('markers', 'id', '*',
                function(response, status){
                  if (status == "success"){
                  	markers.loadData(response);
                  }
                  else
                    console.log("Error in Status");
                });
    });

    $(".netbar-btn-prev").click(function() {
        api.previous('markers', 'id', $("#txtId").val(), '*',
                function(response, status){
                  if (status == "success")
                    markers.loadData(response);
                  else
                    console.log("Error in Status");
                });
    });

    $(".netbar-btn-next").click(function() {
        api.next('markers', 'id', $("#txtId").val(), '*',
                function(response, status){
                  if (status == "success")
                    markers.loadData(response);
                  else
                    console.log("Error in Status");
                });
    });

    $(".netbar-btn-last").click(function() {
        api.last('markers', 'id', '*',
                function(response, status){
                  if (status == "success")
                    markers.loadData(response);
                  else
                    console.log("Error in Status");
                });
    });

    $(".netbar-btn-search").click(function() {
    	bootbox.alert("Searching");
    });

    $(".netbar-btn-add").click(function() {
    	if (netBar.getStatus() == 1) {
    		// Adding
    		netBar.setStatus(2);
    		markers.cleanWidgets();
    		markers.disableWidgets(false);
    		netBar.disableNav();
    	}
    });

    $(".netbar-btn-edit").click(function() {
    	if (netBar.getStatus() == 1) {
    		// Editing
        	netBar.setStatus(3);
        	markers.disableWidgets(false);
    		netBar.disableNav();
    	}
    	else {
    		markers.save();
    		markers.disableWidgets(true);
    		netBar.enableNav();
    	}
    });

    $(".netbar-btn-delete").click(function() {
    	if (netBar.getStatus() == 1) {
    		bootbox.confirm("Want you delete marker?", 
				function(result){
					if (result)
						markers.delete();
				});
    	}
    	else{
    		$(".netbar-btn-last").trigger("click");
    		markers.disableWidgets(true);
    		netBar.enableNav();
    	}
    });
}

/**
 * Disable Navigation Buttons
 */
netBar.disableNav = function(){
	$(".netbar-btn-first").prop('disabled', true);
	$(".netbar-btn-prev").prop('disabled', true);
	$(".netbar-btn-next").prop('disabled', true);
	$(".netbar-btn-last").prop('disabled', true);
	$(".netbar-btn-add").prop('disabled', true);
	$(".netbar-btn-search").prop('disabled', true);

	$(".netbar-btn-edit").removeClass('glyphicon-pencil');
	$(".netbar-btn-edit").removeClass('btn-info');
	$(".netbar-btn-edit").addClass('glyphicon-floppy-disk');
	$(".netbar-btn-edit").addClass('btn-success');
	$(".netbar-btn-edit").html(" Save");

	$(".netbar-btn-delete").removeClass('glyphicon-trash');
	$(".netbar-btn-delete").removeClass('btn-warning');
	$(".netbar-btn-delete").addClass('glyphicon-remove');
	$(".netbar-btn-delete").addClass('btn-danger');
	$(".netbar-btn-delete").html(" Cancel");
}

/**
 * Enable Navigation Buttons
 */
netBar.enableNav = function(){
	$(".netbar-btn-first").prop('disabled', false);
	$(".netbar-btn-prev").prop('disabled', false);
	$(".netbar-btn-next").prop('disabled', false);
	$(".netbar-btn-last").prop('disabled', false);
	$(".netbar-btn-add").prop('disabled', false);
	$(".netbar-btn-search").prop('disabled', false);

	$(".netbar-btn-edit").removeClass('glyphicon-floppy-disk');
	$(".netbar-btn-edit").removeClass('btn-success');
	$(".netbar-btn-edit").addClass('glyphicon-pencil');
	$(".netbar-btn-edit").addClass('btn-info');
	$(".netbar-btn-edit").html(" Edit");

	$(".netbar-btn-delete").removeClass('glyphicon-remove');
	$(".netbar-btn-delete").removeClass('btn-danger');
	$(".netbar-btn-delete").addClass('glyphicon-trash');
	$(".netbar-btn-delete").addClass('btn-warning');
	$(".netbar-btn-delete").html(" Delete");

	// Navigating
	netBar.setStatus(1);
}