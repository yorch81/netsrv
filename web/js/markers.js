/**
 * Manage Markers
 */
function markers(){

}

/**
 * Load data in Widgets
 * 
 * @param  JSON data JSON Data
 */
markers.loadData = function(data){
	$("#txtId").val(data.Table[0].id);
    $("#txtName").val(data.Table[0].name);
    $("#txtAddress").val(data.Table[0].address);
    $("#txtLatitude").val(data.Table[0].lat);
    $("#txtLongitude").val(data.Table[0].lng);
}

/**
 * Disable Widgets
 * 
 * @param  boolean yesNo true for disable, false for enable
 */
markers.disableWidgets = function(yesNo){
	$("#txtName").prop('disabled', yesNo);
    $("#txtAddress").prop('disabled', yesNo);
    $("#txtLatitude").prop('disabled', yesNo);
    $("#txtLongitude").prop('disabled', yesNo);
}

/**
 * Clean Widgets
 */
markers.cleanWidgets = function(){
	$("#txtId").val(0);
    $("#txtName").val("");
    $("#txtAddress").val("");
    $("#txtLatitude").val(0);
    $("#txtLongitude").val(0);
}

/**
 * Save Marker
 */
markers.save = function(){
	var jsonParams = {'pId':$("#txtId").val(),
    				  'pName':$("#txtName").val(),
    				  'pAddress':$("#txtAddress").val(),
    				  'pLat':$("#txtLatitude").val(),
    				  'pLng':$("#txtLongitude").val()};

   	if (netBar.getTable() == 'markers') {
   		api.procedure('usp_savemarker', jsonParams,
                function(response, status){
                  if (status == "success"){
                  	// Load Data
              		  markers.loadData(response);             
                  }
                  else
                    console.log("Error in Status");
                });
   	}
   	else{
      var jsonParamsSQL = {'@id':$("#txtId").val(),
                  '@name':$("#txtName").val(),
                  '@address':$("#txtAddress").val(),
                  '@lat':$("#txtLatitude").val(),
                  '@lng':$("#txtLongitude").val()};

      api.procedure('dbo.usp_savemarker', jsonParamsSQL,
                function(response, status){
                  if (status == "success"){
                    // Load Data
                    markers.loadData(response);             
                  }
                  else
                    console.log("Error in Status");
                });
    }   
}

/**
 * Delete Marker
 */
markers.delete = function(){
	var jsonParams = {'pId':$("#txtId").val()};

	if (netBar.getTable() == 'markers') {
		api.procedure('usp_delmarker', jsonParams,
                function(response, status){
                  if (status == "success"){
                  	// Move Next
              		api.next('markers', 'id', $("#txtId").val(), '*',
	                    function(response, status){
	                      if (status == "success")
	                        markers.loadData(response);
	                      else
	                        console.log("Error in Status");
	                    });              
                  }
                  else
                    console.log("Error in Status");
                });
	}
	else{
    var jsonParamsSQL = {'@id':$("#txtId").val()};

    api.procedure('dbo.usp_delmarker', jsonParamsSQL,
                function(response, status){
                  if (status == "success"){
                    // Move Next
                  api.next('markers', 'id', $("#txtId").val(), '*',
                      function(response, status){
                        if (status == "success")
                          markers.loadData(response);
                        else
                          console.log("Error in Status");
                      });              
                  }
                  else
                    console.log("Error in Status");
                });
  }
}

