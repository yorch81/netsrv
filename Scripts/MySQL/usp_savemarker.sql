DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `usp_savemarker`(pId INT(11), 
pName VARCHAR(60), pAddress VARCHAR(80), pLat FLOAT(10,6), pLng FLOAT(10,6))
BEGIN
	IF (pId = 0) THEN
		INSERT INTO markers (name, address, lat, lng)
		VALUES (pName, pAddress, pLat, pLng);

		SELECT * FROM markers WHERE id = LAST_INSERT_ID();
	ELSE
		UPDATE markers 
		SET name = pName, 
			address = pAddress, 
			lat = pLat,
			lng = pLng
		WHERE id = pId;

		SELECT * FROM markers WHERE id = pId;
	END IF;
END$$
DELIMITER ;
