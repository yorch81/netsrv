DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `usp_delmarker`(pId int(11))
BEGIN
	DELETE FROM markers WHERE id = pId;

	SELECT "OK" AS MSJ;
END$$
DELIMITER ;
