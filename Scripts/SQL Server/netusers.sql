USE MARKERS
GO

/****** Object:  Table [dbo].[netusers]    Script Date: 10/26/2015 11:42:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[netusers](
	[id] [numeric](11, 0) IDENTITY(1,1) NOT NULL,
	[nuser] [varchar](60) NOT NULL,
	[npassword] [varchar](80) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


