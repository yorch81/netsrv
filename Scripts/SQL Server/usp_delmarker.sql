USE MARKERS
GO
/****** Object:  StoredProcedure [dbo].[usp_delmarker]    Script Date: 07/02/2015 18:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_delmarker] 
	@id NUMERIC(11,0) = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @id > 0 BEGIN		
		DELETE FROM markers WHERE id = @id;
	END 
	
	SELECT 'OK' AS MSJ;
END -- SP

