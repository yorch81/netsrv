USE MARKERS
GO
/****** Object:  StoredProcedure [dbo].[usp_savemarker]    Script Date: 07/02/2015 18:24:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_savemarker] 
	@id NUMERIC(11,0) = 0,
	@name VARCHAR(60) = '', 
	@address VARCHAR(80) = '',
	@lat NUMERIC(10,6),
	@lng NUMERIC(10,6)
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @id = 0 BEGIN		
		INSERT INTO markers (name, address, lat, lng)
		VALUES (@name, @address, @lat, @lng);

		SELECT * FROM markers WHERE id = @@IDENTITY;
	END
	ELSE BEGIN
	
		UPDATE markers 
		SET name = @name, 
			address = @address, 
			lat = @lat,
			lng = @lng
		WHERE id = @id;

		SELECT * FROM markers WHERE id = @id;
	END 
	
	SELECT 'OK' AS MSJ;
END -- SP

