USE MARKERS
GO

/****** Object:  Table [dbo].[markers]    Script Date: 07/02/2015 18:24:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[markers](
	[id] [numeric](11, 0) IDENTITY(1,1) NOT NULL,
	[name] [varchar](60) NOT NULL,
	[address] [varchar](80) NOT NULL,
	[lat] [numeric](10, 6) NOT NULL,
	[lng] [numeric](10, 6) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


