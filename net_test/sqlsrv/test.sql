USE [MARKERS]
GO

/****** Object:  Table [dbo].[test]    Script Date: 03/08/2016 02:44:41 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[test](
	[id] [numeric](12, 0) IDENTITY(1,1) NOT NULL,
	[number] [numeric](12, 0) NOT NULL,
	[today] [datetime] NOT NULL CONSTRAINT [DF_test_today]  DEFAULT (getdate())
) ON [PRIMARY]

GO


