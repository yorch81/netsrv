USE [MARKERS]
GO

/****** Object:  Table [dbo].[test_v3]    Script Date: 02/08/2016 11:49:34 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[test_v3](
	[_id] [numeric](12, 0) IDENTITY(1,1) NOT NULL,
	[number] [numeric](12, 0) NOT NULL,
	[today] [varchar](30) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


