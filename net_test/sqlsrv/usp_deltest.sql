USE [MARKERS]
GO

/****** Object:  StoredProcedure [dbo].[usp_deltest]    Script Date: 03/08/2016 02:46:02 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[usp_deltest] 
	@number NUMERIC(12,0) = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE FROM	dbo.test WHERE number = @number;
	
	SELECT @@ROWCOUNT AS TOTAL;
END -- SP





GO


