USE [MARKERS]
GO

/****** Object:  StoredProcedure [dbo].[usp_updtest]    Script Date: 03/08/2016 02:45:50 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[usp_updtest] 
	@number NUMERIC(12,0) = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE dbo.test SET today=GETDATE() WHERE number = @number;
	
	SELECT @@ROWCOUNT AS TOTAL;
END -- SP




GO


