USE [MARKERS]
GO

/****** Object:  StoredProcedure [dbo].[usp_instest]    Script Date: 03/08/2016 02:45:30 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[usp_instest] 
	@number NUMERIC(12,0) = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO dbo.test (number, today) VALUES (@number, GETDATE());
	
	SELECT 'OK' AS MSG;
END -- SP




GO


