DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `usp_deltest`(pNumber int(12))
BEGIN
	DELETE FROM test WHERE number=pNumber;
    
    SELECT ROW_COUNT() AS TOTAL;
END$$
DELIMITER ;
