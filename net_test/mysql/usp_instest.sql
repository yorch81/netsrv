DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `usp_instest`(pNumber int(12))
BEGIN
	INSERT INTO test (number, today) values (pNumber, NOW());
    
    SELECT 'OK' AS MSG;
END$$
DELIMITER ;
