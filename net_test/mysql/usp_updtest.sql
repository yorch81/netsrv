DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `usp_updtest`(pNumber int(12))
BEGIN
	UPDATE test SET today=NOW() WHERE number=pNumber;
    
    SELECT ROW_COUNT() AS TOTAL;
END$$
DELIMITER ;
